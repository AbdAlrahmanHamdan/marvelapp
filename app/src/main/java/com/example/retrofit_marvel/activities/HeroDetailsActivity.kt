package com.example.retrofit_marvel.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.retrofit_marvel.R
import com.example.retrofit_marvel.dataholders.Hero
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_hero.*

class HeroDetailsActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hero)
        val hero =  intent.getParcelableExtra<Hero>("HeroObject")
        Picasso.get().load(hero!!.imageurl).into(heroImage)
        nameValue.text = hero.name
        realNameValue.text = hero.realname
        teamValue.text = hero.team
        firstAppearanceValue.text = hero.firstappearance
        creatorValue.text = hero.createdby
        publisherValue.text = hero.publisher
        bioValue.text = hero.bio
    }
}