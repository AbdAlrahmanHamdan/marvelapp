package com.example.retrofit_marvel.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit_marvel.R
import com.example.retrofit_marvel.adapters.HeroRecyclerViewAdapter
import com.example.retrofit_marvel.dataholders.Hero
import com.example.retrofit_marvel.delegates.HeroRecyclerViewAdapterDelegate
import com.example.retrofit_marvel.utils.Api
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(),
    HeroRecyclerViewAdapterDelegate {
    var heroRecyclerViewHolder: RecyclerView? = null
    var heroList: List<Hero?>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        heroRecyclerViewHolder = heroRecyclerView
        heroRecyclerViewHolder!!.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val retrofit = Retrofit.Builder().baseUrl(Api.BASE_URL)
                                                  .addConverterFactory(GsonConverterFactory.create())
                                                  .build()
        val api = retrofit.create(Api::class.java)
        val call: Call<List<Hero?>?>? = api.getHeroes()

        call!!.enqueue(object : Callback<List<Hero?>?> {
            override fun onResponse(call: Call<List<Hero?>?>, response: Response<List<Hero?>?>) {
                heroList = response.body()
                val heroRecyclerAdapter =
                    HeroRecyclerViewAdapter(
                        this@MainActivity,
                        heroList
                    )
                heroRecyclerAdapter.heroRecyclerViewAdapterDelagte = this@MainActivity
                heroRecyclerViewHolder!!.adapter = heroRecyclerAdapter

            }

            override fun onFailure(
                call: Call<List<Hero?>?>, t: Throwable) {
                    Log.i("Fail", "Fail")
            }
        })
    }

    override fun openHeroDetailsActivity(hero: Hero) {
        val heroDetailsActivity = Intent(this@MainActivity, HeroDetailsActivity::class.java)
        heroDetailsActivity.putExtra("HeroObject", hero)
        startActivity(heroDetailsActivity)
    }
}
