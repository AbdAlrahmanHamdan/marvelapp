package com.example.retrofit_marvel.dataholders

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Hero (var name: String, var realname: String, var team: String, var firstappearance: String, var createdby: String, var publisher: String, var imageurl: String, var bio: String): Parcelable