package com.example.retrofit_marvel.dataholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit_marvel.delegates.HeroDelegate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_hero.view.*

class HeroViewHolder(view: View?): RecyclerView.ViewHolder(view!!), View.OnClickListener {
    var heroDelegate: HeroDelegate? = null
    var hero: Hero? = null
    init{
        itemView.setOnClickListener(this)
        itemView.rootView.setOnClickListener {
            heroDelegate?.setDetails(hero!!)
        }
    }

    override fun onClick(p0: View?) {

    }

    fun setData(heroItem: Hero?) {
        itemView.heroItemNameValue.text = heroItem!!.name
        itemView.heroItemRealNameValue.text = heroItem!!.realname
        itemView.heroItemTeamValue.text = heroItem!!.team
        Picasso.get().load(heroItem.imageurl).into(itemView.heroItemImage)
    }
}