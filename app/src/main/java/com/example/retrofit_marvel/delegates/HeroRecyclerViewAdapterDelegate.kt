package com.example.retrofit_marvel.delegates

import com.example.retrofit_marvel.dataholders.Hero

interface HeroRecyclerViewAdapterDelegate {
    fun openHeroDetailsActivity(hero: Hero)
}