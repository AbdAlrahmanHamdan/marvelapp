package com.example.retrofit_marvel.delegates

import com.example.retrofit_marvel.dataholders.Hero

interface HeroDelegate {
    fun setDetails(hero: Hero)
}