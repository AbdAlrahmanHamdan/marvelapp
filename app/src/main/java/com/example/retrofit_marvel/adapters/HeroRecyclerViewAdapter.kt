package com.example.retrofit_marvel.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit_marvel.*
import com.example.retrofit_marvel.dataholders.Hero
import com.example.retrofit_marvel.dataholders.HeroViewHolder
import com.example.retrofit_marvel.delegates.HeroDelegate
import com.example.retrofit_marvel.delegates.HeroRecyclerViewAdapterDelegate

class HeroRecyclerViewAdapter (private val context: Context?, private val Heroes: List<Hero?>?): RecyclerView.Adapter <HeroViewHolder>(),
    HeroDelegate {
    var heroRecyclerViewAdapterDelagte: HeroRecyclerViewAdapterDelegate? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.item_hero, parent,false)
        return HeroViewHolder(
            layoutInflater
        )
    }

    override fun getItemCount(): Int {
        return Heroes!!.size
    }

    override fun onBindViewHolder(holder: HeroViewHolder, position: Int) {
        val heroItem = Heroes?.get(position)
        holder.setData(heroItem)
        holder.hero = heroItem
        holder.heroDelegate = this
    }

    override fun setDetails(hero: Hero) {
        heroRecyclerViewAdapterDelagte?.openHeroDetailsActivity(hero)
    }
}