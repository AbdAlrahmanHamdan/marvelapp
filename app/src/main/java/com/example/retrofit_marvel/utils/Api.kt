package com.example.retrofit_marvel.utils

import com.example.retrofit_marvel.dataholders.Hero
import retrofit2.Call
import retrofit2.http.GET


interface Api {
    @GET("marvel")
    open fun getHeroes(): Call<List<Hero?>?>?

    companion object {
        const val BASE_URL = "https://simplifiedcoding.net/demos/"
    }
}